#!/usr/bin/env python3


import pytest

from fdsn_doi_metrics import fdsn_doi_metrics

def test_fetch_stationxml():
    staxml = fdsn_doi_metrics.fetch_stationxml("https://ws.resif.fr/fdsnws/station/1/query?level=network&format=xml")
    print(staxml)
