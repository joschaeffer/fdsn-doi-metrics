# Metrics collector for DOI in the FDSN

This project collects metrics about [FDSN](https://fdsn.org) seismological network citation metadata (DOI).

## Usage

Installation with poetry:

    poetry install
    
Run the program:

    python fdsn_doi_metrics/fdsn_doi_metrics.py

## List of metrics


1. percent of networks having DOI
2. percent of networks having a license
3. percent of DOI announced in stationXML metadata
4. number of citations for all DOI


All metrics must be computed for different organizations: EIDA, Earthscope, and the rest.

1, 2 and 3 are presented  by year, considering the start year of a network.

# Methodology

The idea is to evaluate the findability of the data throgh all the process of a user (or a robot) searching for data.

## Get all networks known by the FDSN

As a seismological network is always declared at this central point, this is the entry point in order to have a check for each network.

The valuable informations are:
- short network code
- start year
- end year (if exists)
- doi (if exists)

## Fetch datacite metadata

For each network having a DOI known by FDSN, we want to check if the DataCite metadata has a licence.

## Find the authoritative data center

As of today, there are several sources to get this information. At an international level, there is an intention (and actual efforts) to make FDSN datacenter web service the unique source of information, but we are not here yet.

So we do this:
- Get all the networks referenced by FDSN's datacenter web service
- For each network, map to the authoritative datacenter as exposed by FDSN
- Get all networks referenced by EIDA's routing service
- For each network, map to the authoritative datacenter as exposed by EIDA. This step can overwrite information from previous step, because we know that EIDA knwos better about it's data.

## Get stationXml metadata

When each network knows about the authoritative data center, the program fetches the stationXml metadata to check the presence of the DOI in the `Identtifier` tag.


# Output

- CSV data
- Precomputed graphics

