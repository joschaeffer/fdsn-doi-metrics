#!/bin/env python
import json
import logging
import re
from datetime import date
from urllib.parse import urlparse

import pandas as pd
import requests
import untangle
from alive_progress import alive_bar

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("fdsn_doi_stats")
data_file = "networks-metrics.csv"
collected_data = pd.DataFrame(columns=["network", "startyear", "endyear",
                                       "doi", "licence", "stationxml",
                                       "link", "organization", "dmc",
                                       "baseurl", "licences"], dtype=str)
eida_nodes = ['BGR', 'ETH', 'GEOFON', 'ICGC', 'INGV', 'KOERI',
              'LMU', 'NIEP', 'NOA', 'ODC', 'RESIF', 'UIB-NORSAR']


def get_networks_with_doi():
    """
    This step gets all seismological networks at FDSN and populates the
    `collected_data` DataFrame.
    The collected information, that will be matched with other sources
    of information are:
    - short network code
    - start year
    - end year
    - doi
    """
    fdsnurl = "https://fdsn.org/ws/networks/1/query"
    result = requests.get(fdsnurl)
    nets = json.loads(result.content)

    logger.info("Getting networks list at FDSN")
    for net in nets['networks']:
        doi = net['doi'] if len(net['doi']) > 3 else ""
        endyear = net['end_date'][:4] if net['end_date'] else ""
        collected_data.loc[len(collected_data)] = [net['fdsn_code'],
                                                   net['start_date'][:4],
                                                   endyear,
                                                   doi,
                                                   "", "", "", "", "", "", ""]


def get_licence_in_doi():
    """
    For each line in the collected_data, if licence is None, fetch the licence
    from datacite.
    If the rightsList attribute is not empty, License is set to True, else to
    False.
    """
    dataciteapi = "https://api.datacite.org/application/vnd.datacite.datacite+json/"
    logger.info("Fetching Datacite metadata for each network")
    with alive_bar(len(collected_data)) as pbar:
        for index, row in collected_data.iterrows():
            if (str(row['doi']) != "nan" or str(row['doi'] != "")):
                result = requests.get(dataciteapi+row['doi'])
                doi = json.loads(result.content)
                if 'rightsList' in doi.keys() and len(doi['rightsList']) > 0:
                    logger.info(f"Rightslist: {doi['rightsList']}")
                    collected_data.at[index, "licences"] = str(doi['rightsList'])
                    collected_data.at[index, "licence"] = True
            pbar()


def make_eida_nodes_service_map():
    """
    From the FDSN registry, maps the station services with an eida node.
    This function is used to build a reference for the next steps of the
    program, avoiding to request the FDSN for each network.
    """
    eida_nodes_map = {}
    fdsn_datacenter_url = "https://www.fdsn.org/ws/datacenters/1/query?services=fdsnws-station-1"
    result = requests.get(fdsn_datacenter_url)
    datacenters = json.loads(result.content)

    for dmc in datacenters['datacenters']:
        if dmc['name'] in eida_nodes:
            for repo in dmc['repositories']:
                for svc in repo['services']:
                    if svc['name'] == "fdsnws-station-1":
                        k = urlparse(svc['url']).netloc
                        eida_nodes_map[k] = dmc['name']

    logger.debug(eida_nodes_map)
    return eida_nodes_map


def get_eida_authoritative_nodes():
    """
    EIDA routing service knows best about European networks.
    This function sets the "organisation" column to EIDA where suited.
    """
    logger.info("Identifying EIDA networks")
    eida_routing_url = "https://www.orfeus-eu.org/eidaws/routing/query?service=station&format=json"
    result = requests.get(eida_routing_url)
    nodes = json.loads(result.content)

    eida_nodes_map = make_eida_nodes_service_map()

    for node in nodes:
        for net in node['params']:
            if net['priority'] == 1:
                startyear = net['start'][:4]
                # Get index of the corresponding network
                idx = get_network_position(net['net'], startyear)
                #
                # Store the organization
                collected_data.loc[idx, "organization"] = "EIDA"
                # Add URL to metadata
                collected_data.loc[idx, "baseurl"] = f"{node['url']}?level=network&format=xml"
                try:
                    routingd = date.fromisoformat(net['start'][0:10])
                    collected_data.loc[idx, "routingStartTime"] = routingd
                except Exception as e:
                    logger.warning("%s_%s Routing date %s could not be parsed: %s", net["net"], startyear, net['start'], e)
                try:
                    if 'end' in net.keys() and net['end'] != '':
                        routingd = date.fromisoformat(net['end'][0:10])
                        collected_data.loc[idx, "routingEndTime"] = routingd
                except Exception as e:
                    logger.warning("%s_%s Routing end date %s could not be parsed: %s", net["net"], startyear, net['end'], e)
                try:
                    netloc = urlparse(node['url']).netloc
                    collected_data.loc[idx, "dmc"] = eida_nodes_map[netloc]
                except Exception as e:
                    logger.error("No key %s in %s: %s", node['url'], eida_nodes_map, e)


def get_fdsn_authoritative_nodes():
    """
    Get all nodes from FDSN service and for each of them, find the routing.
    """
    logger.info("Listing all DMCs at FDSN")
    fdsn_datacenter_url = "https://www.fdsn.org/ws/datacenters/1/query?services=*-station-1"
    result = requests.get(fdsn_datacenter_url)
    datacenters = json.loads(result.content)

    for dmc in datacenters['datacenters']:
        if dmc['name'] not in eida_nodes:
            logger.info("Got datacenter %s", dmc['name'])
            get_fdsn_authoritative_node(dmc['name'])


def build_station_url(service_url):
    if service_url.endswith("/"):
        service_url = service_url[:-1]
    if not service_url.endswith("/1"):
        service_url = service_url + "/1"
    service_url = service_url + "/query?level=network&format=xml"
    return service_url


def get_network_position(net, year):
    """
    Find position in network comparing:
    - network name if it is a permanent network
    - net name + year if it's a temporary network
    Return position in the dataframe
    """
    idx = -1
    if re.match(r'^[A-W]', net):
        idx = collected_data.loc[(collected_data["network"] == net)].index
        idx = idx[0]
        if collected_data.at[idx, "startyear"] != year:
            logger.warning("%s : Startyear at FDSN (%s) and in routing (%s) mismatch.",
                           net, year, collected_data.at[idx, "startyear"])
    else:
        idx = collected_data.loc[(collected_data["network"]==net) & (collected_data["startyear"]==year)].index
    if idx < 0:
        logger.warning("No network found for %s_%s", net, year)
    return idx


def get_fdsn_authoritative_node(node):
    """
    From FDSN datacenters service, get the list of networks for one node.
    """
    logger.info("Identifying %s networks", node)
    fdsn_datacenter_url = f"https://www.fdsn.org/ws/datacenters/1/query?services=*-station-1&includeDatasets=True&name={node}"
    result = requests.get(fdsn_datacenter_url)
    datacenter = json.loads(result.content)

    for rep in datacenter["datacenters"][0]["repositories"]:
        logger.info("  Browsing repository %s in %s", rep["name"], node)
        for net in rep["datasets"]:
            if net['priority'] == 1:
                startyear = net['starttime'][:4]
                service_url = build_station_url(rep["services"][0]["url"])
                idx = get_network_position(net['network'], startyear)
                if idx >= 0:
                    collected_data.loc[idx, "dmc"] = node
                    collected_data.loc[idx, "baseurl"] = service_url
                try:
                    routingd = date.fromisoformat(net['starttime'][0:10])
                    collected_data.loc[idx, "routingStartTime"] = routingd
                except Exception:
                    logger.warning("%s:%s_%s: Routing start date %s could not be parsed",
                                   node, net['network'], startyear, net['starttime'])
                try:
                    if 'endtime' in net.keys():
                        routingd = date.fromisoformat(net['endtime'][0:10])
                        collected_data.loc[idx, "routingEndTime"] = routingd
                except Exception:
                    logger.warning("Routing end date %s could not be parsed",
                                   net['endtime'])


def fetch_stationxml(url):
    try:
        logger.info("Requesting stationxml metadata at %s", url)
        stationxml = untangle.parse(url)
    except Exception as e:
        logger.error("Error getting stationXML metadata at %s", url)
        logger.error(e)
        raise e
    return stationxml


def get_doi_in_stationxml():
    """
    For each network, asks the FDSN station webservice at authoritative node
    and search for the DOI in it.
    If metadata is not found, set "stationxml" attribute to False
    If DOI is not found, set stationxml attribute to True and link attribute
    to False
    Also, some sanity checks are done and warning are issued in the following
    cases:
    - Unmatched DOI between FDSN and StationXML
    - Unmatched start date between Routing (FDSN or EIDA) and StationXML
    - Unmatched end date between Routing (FDSN or EIDA) and StationXML
    """
    stationxml_cache = {}
    with alive_bar(len(collected_data)) as pbar:
        for index, row in collected_data.iterrows():
            logger.debug("===\n   %s",row)
            # If there is an stationxml endpoint defined
            if str(row.baseurl).startswith('http'):
                # We build a cache containing all stationXML data for each endpoint.
                if row.baseurl not in stationxml_cache.keys():
                    try:
                        stationxml_cache[row.baseurl] = fetch_stationxml(row.baseurl)
                    except Exception:
                        logger.error("Cache not constituted. Next")
                        continue
                # Get all networks
                # TODO Tester l'année seulement pour les réseaux tempo
                # TODO Ajouter quand même un warning dans les logs si l'année n'est pas la même
                nets = [n for n in stationxml_cache[row.baseurl].FDSNStationXML.Network
                         if  n['code'] == row.network and (re.match(r'^[A-W]', n['code'] ) or n['startDate'][:4] == row.startyear) ]
                if len(nets) > 0:
                    logger.debug("    Network defined in stationXML")
                    # Set the stationxml value to True
                    collected_data.at[index, "stationxml"] = True
                    doi = str(row.doi)
                    # If the DOI looks like a valid one
                    if doi != "" and doi != "nan":
                        logger.debug("    search for DOI %s", doi)
                        # Look if it appears in the stationxml string
                        stationxml_doi = ""
                        try:
                            stationxml_doi = nets[0].Identifier.cdata.strip()
                        except Exception as e:
                            logger.debug("    DOI %s not found: %s", doi, e)
                            collected_data.at[index, "link"] = False
                        if stationxml_doi.lower() == doi.lower():
                            collected_data.at[index, "link"] = True
                        else:
                            if stationxml_doi != "":
                                logger.warning("    DOI at FDSN (%s) does not match DOI in stationXML (%s)", doi, stationxml_doi)
                    else:
                        logger.debug("    DOI seems empty: %s", doi)
                    # Store dates
                    if nets[0]['startDate']:
                        collected_data.at[index, "stationXMLStartTime"] = date.fromisoformat(nets[0]['startDate'][0:10])
                    if nets[0]['endDate']:
                        collected_data.at[index, "stationXMLEndTime"] = date.fromisoformat(nets[0]['endDate'][0:10])
                # The network could not be found.
                # Set the stationxml value to False
                else:
                    logger.debug("    Network NOT defined in stationXML")
                    collected_data.at[index, "stationxml"] = False
                    collected_data.at[index, "link"] = ""
            else:
                collected_data.at[index, "stationxml"] = ""
            pbar()


def save_metrics():
    """
    Write all metrics file to CSV format
    """
    f = open(data_file, 'w')
    f.write("# This file is available under the CC-by 4.0 licence\n")
    f.write(f"# Generated by at { date.today() } by fdsn-doi-metrics (https://gitlab.com/joschaeffer/fdsn-doi-metrics/)\n")
    f.write("# Attributes description:\n")
    f.write("# Network, startyear, endyear: selfexplanatory\n")
    f.write("# doi: empty if no DOI known at FDSN. Else value is the DOI\n")
    f.write("# licence: False if no licence defined in datacite metadata, True if at least on licence is found.\n")
    f.write("# stationxml: True if stationxml is correctly served by authoritative data center. False otherwise.\n")
    f.write("# link: True if DOI is advertised in stationXML. False if it is not, empty if stationXML request did not succeed.\n")
    f.write("# organization: EIDA or Other\n")
    f.write("# baseurl: The station webservice endpoint used\n")
    f.write("# dmc: The data management center serving this network with priority=1\n")
    f.write("#\n")

    collected_data.to_csv(f, index=False)


if __name__ == "__main__":
    try:
        collected_data = pd.read_csv(data_file, comment='#', keep_default_na=False,
                                     dtype={'network':str, 'startyear':str, 'endyear':str, 'doi': str,
                                            'licence': str, 'stationxml': str, 'link': str, 'organization': str,
                                            'baseurl': str, 'dmc': str, 'licences': str})
        logger.info("Data sucessfully imported from %s",  data_file)
    except Exception:
        logger.info(f"No data found in {data_file} fetch networks from FDSN ws")
        get_networks_with_doi()

        get_fdsn_authoritative_nodes()
        get_eida_authoritative_nodes()
    get_licence_in_doi()
    # get_doi_in_stationxml()
    print(collected_data)
    save_metrics()
